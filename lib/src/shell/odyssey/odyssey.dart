import 'package:go_router/go_router.dart';
import 'package:yff/src/ui/views/kickoff/kickoff.dart';

part 'routes.dart';

class Odyssey {
  static GoRouter get odyssey => _odyssey;
  static final GoRouter _odyssey = GoRouter(
    routes: [
      GoRoute(
        path: Routes.kickoff,
        name: Routes.kickoff,
        builder: (context, state) => const Kickoff(),
      ),
    ],
  );
}
