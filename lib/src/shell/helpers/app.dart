import 'package:package_info_plus/package_info_plus.dart';
import 'package:yff/src/shell/di/di.dart';

String get appName => di<PackageInfo>().appName;

String get appVersion => di<PackageInfo>().version;

String get appBuildNumber => di<PackageInfo>().buildNumber;
