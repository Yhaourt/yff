import 'package:logger/logger.dart';
import 'package:yff/src/shell/di/di.dart';

Logger get logger => di<Logger>();
