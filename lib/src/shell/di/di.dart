import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:package_info_plus/package_info_plus.dart';

final GetIt di = GetIt.instance;

class DependencyInjection {
  static Future<void> init() async {
    di.registerLazySingleton(
      () => Logger(
        printer: PrettyPrinter(),
      ),
    );

    di.registerSingletonAsync<PackageInfo>(
      () => PackageInfo.fromPlatform(),
    );

    await di.allReady(
      timeout: const Duration(seconds: 10),
    );
  }
}
