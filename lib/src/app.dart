import 'package:flutter/material.dart';
import 'package:yff/src/shell/helpers/app.dart';
import 'package:yff/src/shell/odyssey/odyssey.dart';
import 'package:yff/src/ui/theme/aesthetic.dart';

class App extends StatelessWidget {
  const App({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: appName,
      routerConfig: Odyssey.odyssey,
      theme: Aesthetic.theme,
    );
  }
}
