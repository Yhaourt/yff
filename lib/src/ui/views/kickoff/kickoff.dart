import 'package:flutter/material.dart';
import 'package:yff/src/shell/helpers/app.dart';
import 'package:yff/src/ui/theme/aesthetic.dart';

class Kickoff extends StatelessWidget {
  const Kickoff({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: EdgeInsets.all(Layout.lPadding),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                'https://i.imgur.com/XDCihO1.png',
                width: 100,
                height: 100,
              ),
              SizedBox(height: Layout.spacing),
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(Layout.padding),
                decoration: BoxDecoration(
                  color: Aesthetic.theme.colorScheme.surface,
                  borderRadius: BorderRadius.circular(Layout.borderRadius),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '✅ $appName is running !',
                      style: TextStyle(
                        fontSize: 18,
                        color: Aesthetic.theme.colorScheme.onSurface,
                      ),
                    ),
                    SizedBox(height: Layout.spacing),
                    Text(
                      'You can now create your first page with :',
                      style: TextStyle(
                        color: Aesthetic.theme.colorScheme.onSurface,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.all(Layout.padding),
                      decoration: BoxDecoration(
                        color: Aesthetic.theme.colorScheme.surfaceVariant,
                        borderRadius:
                            BorderRadius.circular(Layout.borderRadius),
                      ),
                      child: Text(
                        '\$  yff create page myPage',
                        style: TextStyle(
                          color: Aesthetic.theme.colorScheme.onSurfaceVariant,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
