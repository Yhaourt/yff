import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

part 'custom_colors.dart';

part 'layout.dart';

class Aesthetic {
  static ThemeData get theme => _theme;

  static final ThemeData _baseTheme = ThemeData(
    fontFamily: GoogleFonts.openSans().fontFamily,
    appBarTheme: const AppBarTheme(
      color: Colors.transparent,
      elevation: 0,
    ),
  );

  static final ThemeData _theme = _baseTheme.copyWith(
    scaffoldBackgroundColor: _themeColorScheme().background,
    colorScheme: _themeColorScheme(),
    iconTheme: IconThemeData(
      color: CustomColors.yffDark[800]!,
      size: 24,
    ),
    textTheme: _baseTheme.textTheme.apply(
      bodyColor: CustomColors.yffDark[800]!,
      displayColor: CustomColors.yffDark[800]!,
    ),
  );

  static ColorScheme _themeColorScheme() {
    return ColorScheme(
      brightness: Brightness.dark,
      primary: CustomColors.yffGreen[500]!,
      onPrimary: Colors.white,
      secondary: CustomColors.yffGreen[200]!,
      onSecondary: Colors.white,
      error: Colors.transparent,
      onError: Colors.transparent,
      background: Colors.white,
      onBackground: CustomColors.yffDark[800]!,
      surface: CustomColors.yffDark[300]!,
      onSurface: Colors.white,
      surfaceVariant: CustomColors.yffDark[800]!,
      onSurfaceVariant: Colors.grey[300]!,
    );
  }
}
