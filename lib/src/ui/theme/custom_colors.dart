part of 'aesthetic.dart';

class CustomColors {
  static Map<int, Color> get yffGreen => {
        200: const Color(0xFFA7F3D0),
        500: const Color(0xFF10B981),
      };

  static Map<int, Color> get yffDark => {
        300: const Color(0xFF475569),
        800: const Color(0xFF0F172A),
      };
}
