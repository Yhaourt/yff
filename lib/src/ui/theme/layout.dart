part of 'aesthetic.dart';

class Layout {
  static double get sSpacing => 5;

  static double get spacing => 15;

  static double get lSpacing => 30;

  static double get sBorderRadius => 4;

  static double get borderRadius => 12;

  static double get lBorderRadius => 30;

  static double get sPadding => 8;

  static double get padding => 16;

  static double get lPadding => 24;

  static double get xlPadding => 32;
}
