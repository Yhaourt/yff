import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:yff/src/app.dart';
import 'package:yff/src/shell/di/di.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ),
  );

  // Initialize the dependency injection container.
  await DependencyInjection.init();
  runApp(
    const App(),
  );
}
